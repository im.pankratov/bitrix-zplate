'use strict';

import plugins  from 'gulp-load-plugins';
import yargs    from 'yargs';
import browser  from 'browser-sync';
import gulp     from 'gulp';
import rimraf   from 'rimraf';
import yaml     from 'js-yaml';
import fs       from 'fs';
import proc     from 'child_process';
import pkg      from './package.json';
import crBump   from 'conventional-recommended-bump';

// Load all Gulp plugins into one variable
const $ = plugins();

// Flags
const PRODUCTION = !!(yargs.argv.production);
const SUSPEND    = !!(yargs.argv.suspend);
const TYPE       = yargs.argv.type;

// Load settings from settings.yml
const {FOLDERS, COMPATIBILITY, CONVENTION, ASSETS} = loadConfig('config.yml');
const {DEV, PROD} = loadConfig('servers.yml');

const PATH = {
  server: {
    root:       DEV.www.host,
    local:      DEV.www.host + FOLDERS.local,
    template:   DEV.www.host + FOLDERS.local + 'templates/' + pkg.name + '/',
    components: DEV.www.host + FOLDERS.local + FOLDERS.components,
    watch:      [DEV.www.host, '!' + DEV.www.host + '{bitrix,upload,local,desktop_app}']
  },
  src:          {
    root:       FOLDERS.src,
    template:   FOLDERS.src + FOLDERS.template,
    components: FOLDERS.src + FOLDERS.components,
    assets:     FOLDERS.src + FOLDERS.assets
  }
};

function loadConfig(file) {
  let ymlFile = fs.readFileSync(file, 'utf8');
  return yaml.load(ymlFile);
}

// Execute child process with command
function execute(command, done) {
  proc.exec(command, (error, stdout, stderr) => {
    if (error) {
      console.error(`exec error: ${error}`);
      return done();
    }

    console.log(`stdout: ${stdout}`);
    if (stderr) {
      console.log(`stderr: ${stderr}`);
    }
    return done();
  });
}

// Task chains
const assets     = gulp.parallel(description, sass, javascript, images, fonts, copyComponents, copyTemplate);

const serve      = gulp.series(clean, assets, server, watch);
const release    = gulp.series(getVersion, bumpVersion, updateChangelog, commitChanges, tagVersion);

// Either serve on test Bitrix environment or just create dist archive
gulp.task('default', serve);
gulp.task('release', release);

function clean(done) {
  rimraf(PATH.server.local, done);
}

// TODO: remove this already?
// Virtual machine controlling task
gulp.task('vm', (done) => {
  if (SUSPEND) {
    return vmSuspend(done);
  }
  return vmUp(done);
});

function vmUp(done) {
  var command = 'vagrant up';
  execute(command, done);
}

function vmSuspend(done) {
  var command = 'vagrant suspend';
  execute(command, done);
}

// Fills template description from the config file
function description() {
  return gulp.src(PATH.src.root + 'description.php')
    .pipe($.replace('{{name}}', pkg.name))
    .pipe($.replace('{{desc}}', pkg.description))
    .pipe(gulp.dest(PATH.server.template));
}

// Copies files from "template" folder
function copyTemplate() {
  return gulp.src(PATH.src.template + '**/*', {dot: true})
    .pipe($.changed(PATH.server.template))
    .pipe($.if('*.babel.js', $.sourcemaps.init()))
    .pipe($.if('*.babel.js', $.babel()))
    .pipe($.if('*.babel.js', $.rename({
      basename: 'script'
    })))
    .pipe($.if('*.babel.js', $.sourcemaps.write('.')))
    .pipe(gulp.dest(PATH.server.template));
}

// Copies files from "components" folder
function copyComponents() {
  return gulp.src(PATH.src.components + '**/*', {dot: true})
    .pipe($.changed(PATH.server.components))
    .pipe(gulp.dest(PATH.server.components));
}

// Compile Sass into CSS
// In production CSS is compressed
function sass() {
  return gulp.src(PATH.src.assets + 'template_styles.scss')
    .pipe($.sourcemaps.init())
    .pipe($.sass({
      includePaths: ASSETS.sass
    })
      .on('error', $.sass.logError))
    .pipe($.replace('../images/', 'images/'))
    .pipe($.replace('../fonts/', 'fonts/'))
    .pipe($.autoprefixer({
      browsers: COMPATIBILITY
    }))
    .pipe($.if(PRODUCTION, $.cssnano()))
    .pipe($.if(!PRODUCTION, $.sourcemaps.write()))
    .pipe(gulp.dest(PATH.server.template))
    .pipe(browser.stream());
}

// Combine JavaScript into one file
// In production js is minified
function javascript() {
  return gulp.src(ASSETS.javascript)
    .pipe($.sourcemaps.init())
    .pipe($.babel())
    .pipe($.concat('app.js'))
    .pipe($.if(PRODUCTION, $.uglify()
      .on('error', e => {console.log(e);})
    ))
    .pipe($.if(!PRODUCTION, $.sourcemaps.write()))
    .pipe(gulp.dest(PATH.server.template + 'js/'));
}

// Copies images to the "dist" folder
// In production, the images are compressed
function images() {
  return gulp.src(PATH.src.assets + 'images/**/*')
    .pipe($.if(PRODUCTION, $.imagemin({
      progressive: true
    })))
    .pipe(gulp.dest(PATH.server.template + 'images/'));
}

// Copies fonts to the "dist" folder
function fonts() {
  return gulp.src(PATH.src.assets + 'fonts/**/*')
    .pipe(gulp.dest(PATH.server.template + 'fonts/'));
}

function getVersion(done) {
  crBump({
    preset: CONVENTION.preset
  }, (err, result) => {
    pkg.releaseType = result.releaseType;
    done();
  });
}

function bumpVersion() {
  return gulp.src(['./package.json', './bower.json'])
    .pipe($.bump({
      type: (TYPE) ? TYPE : pkg.releaseType
    }))
    .pipe(gulp.dest('./'));
}

function updateChangelog() {
  return gulp.src('CHANGELOG.md', {
    buffer: false
  })
    .pipe($.conventionalChangelog({
      preset: CONVENTION.preset
    }))
    .pipe(gulp.dest('./'));
}

function commitChanges() {
  return gulp.src('.')
    .pipe($.git.add())
    .pipe($.git.commit('Bumped version number'));
}

function tagVersion() {
  return gulp.src('./package.json')
    .pipe($.tagVersion());
}

// Start a server with BrowserSync to preview the site in
function server(done) {
  browser.init({
    proxy: DEV.name,
    port: DEV.port,
    online: false
  });
  done();
}

// Reload the browser with BrowserSync
function reload(done) {
  browser.reload();
  done();
}

// Watch for changes to static assets, pages, Sass, and JavaScript
function watch() {
  // assets
  gulp.watch(PATH.src.assets + 'scss/**/*.scss', sass);
  gulp.watch(PATH.src.assets + 'js/**/*.js', gulp.series(javascript, reload));
  gulp.watch(PATH.src.assets + 'images/**/*', gulp.series(images, reload));
  gulp.watch(PATH.src.assets + 'fonts/**/*', gulp.series(fonts, reload));
  // other template files
  gulp.watch(PATH.src.template, gulp.series(copyTemplate, reload));
  gulp.watch(PATH.src.components, gulp.series(copyComponents, reload));
  gulp.watch(PATH.server.watch, reload);
}
