# Bitrix Foundation Template

Foundation for Sites Template for Bitrix CMF.
Fork of [Foundation ZURB Template](https://github.com/zurb/foundation-zurb-template).

- Sass compilation and prefixing
- JavaScript concatenation
- BrowserSync proxy server
- For production builds:
  - CSS compression
  - JavaScript compression
  - Image compression

## Installation

To use this template, your computer needs:

- [NodeJS](https://nodejs.org/en/) (0.12 or greater)
- [Git](https://git-scm.com/)

This template can be installed with the Foundation CLI, or downloaded and set up manually.

### Setup

To set up the template, open the `www` folder, where the copy of Bitrix CMF is installed.
From there, open command line and run these commands:

```bash
git clone https://github.com/defking/bitrix-zplate template-name
```

Then install the needed dependencies:

```bash
cd template-name
npm install
bower install
```
#### Configuration

In the `config.yml` file edit the `SERVER` and `TEMPLATE` objects.
Notice that all the template files will be copied to the `SERVER.root` directory, and could potentially overwrite some files.

#### Bitrix settings

For correct work of auto-injection changes in Browser-sync, you need to make sure that concatenation & minification of `*.css` & `*.js` files are disabled in Bitrix.

To do this, navigate to:
```
Bitrix Settings - > Product Settings - > Modules Settings -> Main Module
```
and in the `Settings` tab find the `CSS Optimization` section. Uncheck corresponding options there. Don't forget to apply settings.

### Running & Building

Finally, run `npm start` to run Gulp. Your finished site will be created in a folder called `dist`, viewable at this URL by default:

```
http://localhost:4000
```

To create archive with production-ready template, run `npm run build`. Resulting archive will be saved in `dist/template-name.zip`.
