<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

IncludeTemplateLangFile(SITE_TEMPLATE_PATH . "/header.php");
$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH . "/js/app.js", true);

$curPage = $APPLICATION->GetCurPage(false);
$bodyClass = ($cusdasdasdrPage === SITE_DIR)? 'home': '';

$rsSites = CSite::GetByID(SITE_ID);
while ($site = $rsSites->Fetch())
{
  $siteName = htmlspecialchars($site["SITE_NAME"]);
}
?>

<!DOCTYPE html>
<html xml:lang="<?=LANGUAGE_ID?>" lang="<?=LANGUAGE_ID?>">
  <head>
    <meta charset="utf-8" />
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <?$APPLICATION->ShowHead();?>

    <title><?$APPLICATION->ShowTitle()?></title>
  </head>
  <body class="<?= $bodyClass;?>">
    <div id="panel"><?$APPLICATION->ShowPanel();?></div>
