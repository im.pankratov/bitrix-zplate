<a name="1.0.4"></a>
## [1.0.4](/compare/v1.0.3...v1.0.4) (2016-09-26)



<a name="1.0.3"></a>
## [1.0.3](/compare/v1.0.2...v1.0.3) (2016-09-26)


### Bug Fixes

* **gulp:** remove obsolete dest pipe from images task c915390
* **npm:** move conventional-recommended-bump to dev dependencies 3a08b19



<a name="1.0.2"></a>
## 1.0.2 (2016-09-26)



<a name="1.0.1"></a>
## 1.0.1 (2016-09-24)



<a name="0.1.2"></a>
## 0.1.2 (2016-09-24)



